project(insite-arbor)
cmake_minimum_required(VERSION 3.15)

find_package(arbor REQUIRED)
find_package(libpqxx REQUIRED)

set(CMAKE_CXX_STANDARD 17)

add_library(insite-arbor src/pipeline_backend.cpp)
target_include_directories(insite-arbor PUBLIC include)
target_link_libraries(insite-arbor arbor::arbor libpqxx::pqxx_shared)

add_subdirectory(example)