#include <insite/arbor/pipeline_backend.hpp>
#include <sstream>
#include <iostream>

namespace insite {
namespace arbor {

PipelineBackend::PipelineBackend(const arb::recipe& simulation_recipe,
                                 uint16_t port)
    : database_connection_("postgresql://postgres:postgres@localhost:5432") {
  std::stringstream cell_query;
  cell_query << "INSERT INTO arbor_cell (id) "
             << "VALUES ";
  for (arb::cell_size_type num_cells = simulation_recipe.num_cells(), i = 0;
       i < num_cells; ++i) {
    if (i != 0) {
      cell_query << ',';
    }
    cell_query << '(' << i << ')';
  }
  cell_query << ';';
  pqxx::work txn(database_connection_);
  const auto result = txn.exec0(cell_query.str());
  txn.commit();
}

}  // namespace arbor
}  // namespace insite
