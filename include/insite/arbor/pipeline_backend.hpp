#include <cstdint>
#include <arbor/recipe.hpp>
#include <pqxx/pqxx>
#include <insite/arbor/external/httplib.h>

namespace insite {
namespace arbor {

class PipelineBackend {
 public:
  PipelineBackend(const arb::recipe& simulation_recipe, uint16_t port);

 private:
  httplib::Server server_;
  pqxx::connection database_connection_;
};

}  // namespace arbor
}  // namespace insite